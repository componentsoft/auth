import { storiesOf, moduleMetadata } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { withKnobs, text, number, boolean, array, select, radios, color, date, button } from '@storybook/addon-knobs';
import {AppComponent} from '../app/app.component';
import {AppRoutingModule} from '../app/app-routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '../app/material.module';
import {LoginComponent} from '../app/auth/components/login/login.component';
import {SignUpComponent} from '../app/auth/components/sign-up/sign-up.component';



storiesOf('Auth', module)
  .addDecorator(
    moduleMetadata({
      imports: [
        BrowserModule,
        NoopAnimationsModule,
        MaterialModule
      ],
      declarations: [LoginComponent, SignUpComponent],
    }),
  )
  .addDecorator(withKnobs)
  .add('Login', () => {

    return {
      template: `
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

                <div style="height: 400px; border: 1px solid #000000">
                    <app-login></app-login>
                </div>
`,
      props: {
      },
    };
  })
  .add('SignUp', () => {

    return {
      template: `
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

                <div style="height: 400px; border: 1px solid #000000">
                    <app-sign-up></app-sign-up>
                </div>
`,
      props: {
      },
    };
  })
;
