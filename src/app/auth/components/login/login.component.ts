import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public hide = true;
  public model = {login: '', password: ''};

  constructor() { }

  ngOnInit() {
  }

  login() {
    console.log(this.model);
  }

}
