import { NgModule } from '@angular/core';
import {AuthRoutingModule} from './auth-routing.module';
import {LoginComponent} from './components/login/login.component';
import {MaterialModule} from '../material.module';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [LoginComponent, SignUpComponent],
  imports: [
    AuthRoutingModule,
    FormsModule,
    MaterialModule,
  ]
})
export class AuthModule { }
