import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: 'auth', canActivate: [], resolve: {}, loadChildren: './auth/auth.module#AuthModule'},
  {path: '**', pathMatch: 'full', redirectTo: 'auth'}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
