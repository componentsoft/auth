import {NgModule} from '@angular/core';
import {
    MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatListModule, MatMenuModule,
    MatProgressSpinnerModule, MatSelectModule, MatSidenavModule, MatTabsModule, MatFormFieldModule,
    MatIconModule, MatIconRegistry, MatTableModule, MatSortModule, MatTooltipModule
} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
// import {ICONS} from './app.icons';
import {DragDropModule} from '@angular/cdk/drag-drop';

@NgModule({
    exports: [
        MatButtonModule,
        MatCardModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatProgressSpinnerModule,
        MatSelectModule,
        MatSidenavModule,
        MatTabsModule,
        MatIconModule,
        MatTableModule,
        MatSortModule,
        MatTooltipModule,
        DragDropModule
    ]
})
export class MaterialModule {
    constructor(
        iconRegistry: MatIconRegistry,
        sanitizer: DomSanitizer
    ) {
        // ICONS.forEach(icon => {
        //     iconRegistry.addSvgIcon(
        //         icon,
        //         sanitizer.bypassSecurityTrustResourceUrl(`assets/icon/${icon}.svg`));
        // });
    }
}
